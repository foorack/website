# CubeKrowd Website

This repository contains the source code to the CubeKrowd main website. The
website is built using Jekyll and a customly designed theme by @Foorack. Please
do not steal the theme for another site. I worked a lot on it. The reason this
website is open-sourced is to enable the community to be able to help and
improve their own as well as others experience.

## Contribute
Do you know how to code? CubeKrowd is a community server for everyone. By
contributing to our open-source projects you directly improve the experience
for everyone on the server, no matter if it's just a comment on our issue
tracker or a larger code contribution.

To help out you need to know how to use git. If you don't know then Google it,
there are thousands of tutorials available for free online. To work on this
repository you need to clone it and do the following commands:

```bash
npm install
jekyll serve --host "localhost" --port 80
```

This will launch a web-server [http://localhost/](http://localhost/) running
on port 80. Any changes made to the code will automatically update on the
website after you refresh. The reason to use `jekyll serve` instead of another
server is because jekyll noticies when a file is changed and automatically
recompiles. Tutorials on how to use Jekyll can be found online, Google it. :)

After you have made your changes you need to create a merge-request. CubeKrowd
developers and others will then inspect and give feedback on your changes.
If your code pass all inspections then it will be merged into the `dev` branch.
Your changes will then be available at
[https://new.cubekrowd.net/](https://new.cubekrowd.net/) where it will stay
during a testing period, together with other changes. If the dev website works
perfectly fine without bugs then it will be merged into the `master` branch
and the site will be live at [https://cubekrowd.net/](https://cubekrowd.net/).

## Legal
All repositories are licensed under
AGPL-3.0. We do not have a Contributor-License-Agreement because they are
silly and overkill. By commiting code you agree it is yours and that it is
licensed under AGPL-3.0.