class DataPaginator < Jekyll::Generator
  attr_reader :first_url, :path_format

  def generate(site)
    site.pages.dup.each do |page|
      data_pagination = page.data["data-pagination"]

      if data_pagination != nil && data_pagination["enabled"]
        records = site.data[data_pagination["data"]]
        per_page = data_pagination["paginate"]
        pages = (records.size.to_f / per_page.to_f).ceil

        @first_url = page.url
        @path_format = data_pagination["path"]

        (1..pages).each do |page_num|
          if page_num == 1
            page.pager = DataPager.new(self, page_num, per_page, pages, records)
          else
            pager = DataPager.new(self, page_num, per_page, pages, records)
            new_page = PaginatePage.new(site, page, pager)
            site.pages << new_page
          end
        end
      end
    end
  end
end

class PaginatePage < Jekyll::Page
  def initialize(site, template, pager)
    @site = site
    @base = ''
    @dir  = ''
    @name = template.name
    @relative_path = template.relative_path

    process(@name)

    self.content = template.content
    self.data = template.data.dup
    self.data["permalink"] = pager.path
    self.pager = pager
  end
end


class DataPager
  attr_reader :path

  def initialize(paginator, page, per_page, total_pages, records)
    @paginator = paginator
    @page = page
    @per_page = per_page
    @total_pages = total_pages

    start = (page - 1) * per_page
    stop = [start + per_page, records.size].min - 1
    @records = records[start..stop]

    @previous_page = @page == 1 ? nil : (@page - 1)
    @previous_page_path = format_path(@previous_page)
    @next_page = @page == @total_pages ? nil : (@page + 1)
    @next_page_path = format_path(@next_page)
    @total_records = @records.size
    @path = format_path(@page)
  end

  def format_path(page_num)
    return nil if page_num.nil?
    return @paginator.first_url if page_num == 1
    @paginator.path_format.sub(':num', page_num.to_s)
  end

  def to_liquid
    {
      'page' => @page,
      'per_page' => @per_page,
      'records' => @records,
      'total_records' => @total_records,
      'total_pages' => @total_pages,
      'previous_page' => @previous_page,
      'previous_page_path' => @previous_page_path,
      'next_page' => @next_page,
      'next_page_path' => @next_page_path
    }
  end
end
