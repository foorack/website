---
layout: blog_post
title: "Introducing voice-linked text channels"
category: community
author: Foorack
short: "We have now added voice-linked text channels. Hop on Discord and check it out!"
tags:
  - discord
---

Image from Discodapp's official blog.

Hello everyone, long-time no see! I've been pretty busy with school lately and
I will be until the near future. Because of that, I have not had much time to
do much CK development work however we still got something cool to introduce
today, something many of you have requested since we moved to Discord.
<!--more-->

It isn't always you have a microphone, it isn't always you can talk and at
sometimes you maybe just don't want to talk.  As you may have guessed from the
title, yes, we are introducing voice-linked text channels. What this means is
that every voice channel will have a corresponding text channel. The text
channels are named with the same name as the voice channel with the "-voice"
suffix at the end.

The voice-text-channels are by default hidden and you can only see the
text channel when you are in the corresponding voice channel (much like how
TeamSpeak works). Message history is also disabled for the voice channels to
match the behaviour of TeamSpeak.

I know this was not a big update but I hope you will enjoy it.
