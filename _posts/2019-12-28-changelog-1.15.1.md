---
layout: blog_post
title: "1.15.1 is now live on CubeKrowd!"
category: community
author: Lekro
short: "CubeKrowd is now 1.15.1! Be sure to update your client!"
tags:
  - server
  - update
---

We are pleased to announce that the 1.14.4 and 1.15.1 updates are live on CubeKrowd! We wish to document here all CK-specific player-visible changes in these updates. As usual, please report any bugs you encounter :)

*(image by Brownie1111 - thanks!)*

<!--more-->
## General
- Updated all servers (except PVP) to 1.15.1. Players now need a **1.15.1 client** to connect to CubeKrowd.

## Creative and BuildComp
- Updated WorldEdit to major version 7. This update brings changes in the WorldEdit online help system (`//help`), tab completion, and changes in command names (e.g. `//up` is now `/up`).
- `/nightvis` and `/haste` have now been moved to a different plugin. They no longer require on/off arguments and will automatically toggle the effects.
- Added `/orient <yaw> <pitch>` command to allow players to face in an exact direction.

## Survival1
- Survival1 will be opening at **17:00 UTC on January 1st, 2020**! As usual, this world will be as close to vanilla as possible.
- PrankWars is back! Use `/pw` for more information. Remember to follow the rules :)

## Skyblock
- Skyblock is currently **down for updates**.
- Skyblock will be skipping the 1.13.2 and 1.14.4 updates entirely!
- This is a much bigger update for skyblock than before! We are migrating to an entirely new plugin! Expect new challenges, greenhouses, and more in the near future!

## Minigames
- **CKCoins** are back!
  - You can collect coins by playing MissileWars, and they can be used to buy hats.
  - Use `/bal` to check your balance and `/shop` to buy things :)
  - Support for more minigames and rewards coming soon!
- Donators get a **free dragon head** as a hat!
- Fixed the worst of the lag spikes in MissileWars.

