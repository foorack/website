---
layout: blog_post
title: "Log4j2 security vulnerability and workaround"
category: community
author: Lekro, TraksAG
short: "Log4j2 security vulnerability and workaround."
tags:
  - server
---

**Edit:** Mojang has posted an official announcement regarding this issue, be sure to check it out! You can find the announcement <a href="https://www.minecraft.net/en-us/article/important-message--security-vulnerability-java-edition">here</a>. In case you're interested in the technical details, the exploit is officially known as <a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228">CVE-2021-44228</a>, which you can use as a search term.

We've become aware of a security vulnerability affecting **both** minecraft clients and servers. This is a serious vulnerability. In essence, the exploit works by sending special messages in chat. These messages force your computer to connect to a malicious computer.
Please follow the instructions in this blog post to work around the issue for your Minecraft client.
 
<!--more-->
We've figured out the following:
- If you're running Minecraft on Java 8, we believe the exploit allows anyone to run arbitrary code on your computer.
- If you're running Minecraft on Java 16 and above (which is the case if you're on Minecraft 1.16 and above), this exploit allows anyone to crash your client, or log your IP address (we actually reproduced these).

We've worked around this on CK, so the CK machine isn't affected by the exploit. However, it is still important that you fix it on your end, so the exploit doesn't happen on your computer. Fortunately it's not that hard. Some of y'all have probably heard of this already, and here is how to work around it.

## Working around the vulnerability

Please note that this fix only works for Minecraft 1.17 and above. The basic idea is to add the following "Java argument" to your client:

`-Dlog4j2.formatMsgNoLookups=true`

Here is how to do that on various launchers:

### Vanilla launcher
Go to your "Installations", click on the profile you're using, click on "More options", then add

`-Dlog4j2.formatMsgNoLookups=true`

to the JVM arguments.

<a href="/assets/img/log4j_vanilla.png"><img src="/assets/img/log4j_vanilla.png" class="img-fluid" alt="Vanilla mitigation"></a>

*(click to view larger version)*

<video id="vanillalog4j" preload controls src="/assets/img/log4j_vanilla.mov" class="img-fluid"></video>

### MultiMC
Click "Settings" and then go to the "Java" tab, and write

`-Dlog4j2.formatMsgNoLookups=true`

in the Java arguments.

<a href="/assets/img/log4j_multimc.png"><img src="/assets/img/log4j_multimc.png" class="img-fluid" alt="MultiMC mitigation"></a>

*(click to view larger version)*
